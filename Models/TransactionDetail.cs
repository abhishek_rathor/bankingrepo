﻿using System;

namespace BankingApp.Models
{
    /// <summary>
    /// Transaction Model
    /// </summary>
    public class TransactionDetail : BaseAccount
    {
        /// <summary>
        /// Transaction Id - Unique
        /// </summary>
        public int TransactionId { get; set; }

        /// <summary>
        /// Date of transaction
        /// </summary>
        public DateTime TransactionDate { get; set; }

        /// <summary>
        /// Transaction Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Transaction Amount
        /// </summary>
        public decimal Amount { get; set; }
    }
}
