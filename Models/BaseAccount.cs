﻿namespace BankingApp.Models
{
    /// <summary>
    /// Base Account Model
    /// </summary>
    public class BaseAccount
    {
        /// <summary>
        /// Account Id - unique identifier
        /// </summary>
        public int AccountId { get; set; }

        /// <summary>
        /// Account Name
        /// </summary>
        public string AccountName { get; set; }
    }
}
