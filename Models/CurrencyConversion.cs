﻿namespace BankingApp.Models
{
    /// <summary>
    /// Curreny Response Model
    /// </summary>
    public class CurrencyConversion
    {
        /// <summary>
        /// Is Success
        /// </summary>
        public bool success { get; set; }
        
        /// <summary>
        /// Link to 3rd party terms and conditions
        /// </summary>
        public string terms { get; set; }
        
        /// <summary>
        /// Link to 3rd party privacy statement
        /// </summary>
        public string privacy { get; set; }
        
        /// <summary>
        /// Timestamp of the conversion
        /// </summary>
        public int timestamp { get; set; }
        
        /// <summary>
        /// Souce current
        /// </summary>
        public string source { get; set; }
        
        /// <summary>
        /// Quotes for the conversion
        /// </summary>
        public Quotes quotes { get; set; }
        
        /// <summary>
        /// Error details
        /// </summary>
        public Error error { get; set; }
    }

    /// <summary>
    /// Conversion Quote Model
    /// </summary>
    public class Quotes
    {
        /// <summary>
        /// USD to AUD Conversion
        /// TODO: Since the current api did not support source currency switching so the name of the variable of USDAUD. If supported then there would be other variables as well.
        /// </summary>
        public double USDAUD { get; set; }
    }

    /// <summary>
    /// Error Details Model
    /// </summary>
    public class Error
    {
        /// <summary>
        /// Error Code 
        /// </summary>
        public int code { get; set; }

        /// <summary>
        /// Error Info
        /// </summary>
        public string info { get; set; }
    }
}
