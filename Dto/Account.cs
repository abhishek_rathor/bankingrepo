﻿using BankingApp.Models;

namespace BankingApp.Dto
{
    /// <summary>
    /// Account Model
    /// </summary>
    public class Account : BaseAccount
    {
        /// <summary>
        /// Account Balance
        /// </summary>
        public decimal Balance { get; set; }
    }
}
