﻿using BankingApp.Dto;
using BankingApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace BankingApp.Controllers
{
    [ApiController]
    [Route("/")]
    [Produces("application/json")]
    public class AccountController : ControllerBase
    {
        private readonly ILogger<AccountController> _logger;
        private readonly ICurrencyService _currencyService;
        private readonly ITransactionService _transactionService;

        public AccountController(ILogger<AccountController> logger, ICurrencyService currencyService, ITransactionService transactionService)
        {
            _logger = logger;
            _currencyService = currencyService;
            _transactionService = transactionService;
        }

        /// <summary>
        /// Get the List of Accounts
        /// </summary>
        /// <returns>Account</returns>
        /// <response code="200">On Success</response>
        [HttpGet]
        [Route("/MyBankAccounts")]
        public async Task<IActionResult> GetAccounts()
        {
            using (StreamReader r = new StreamReader("MyBankAccounts.json"))
            {
                string json = r.ReadToEnd();
                IEnumerable<Account> items = JsonSerializer.Deserialize<IEnumerable<Account>>(json);
                return Ok(items);
            }
        }

        /// <summary>
        /// Get the converation rate of source currency to AUD
        /// </summary>
        /// <param name="sourceCurrency">USD</param>
        /// <returns></returns>
        /// <response code="200">On Success</response>
        /// <response code="400">If there was error processing the request</response>
        [HttpGet]
        [Route("/CurrencyConversionRate")]
        public async Task<IActionResult> GetCurrencyConversionRate(string sourceCurrency)
        {
            try
            {
                var result = await _currencyService.GetCurrency(sourceCurrency);
                return Ok(result);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get Account Summary between a date range
        /// </summary>
        /// <param name="fromDate">2021-10-01</param>
        /// <param name="toDate">2021-11-01</param>
        /// <returns></returns>
        /// <response code="200">On Success</response>
        /// <response code="400">Date Range is not provided or if there was error processing the request</response>
        [HttpGet]
        [Route("/BankAccountsSummary")]
        public async Task<IActionResult> GetBankAccountsSummary(string fromDate, string toDate)
        {
            if(string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate))
            {
                return BadRequest("Please enter date range");
            }
            try
            {
                var result = await _transactionService.GetTransactions(Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate));
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
