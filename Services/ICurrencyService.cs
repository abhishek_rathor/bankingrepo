﻿using BankingApp.Models;
using System.Threading.Tasks;

namespace BankingApp.Services
{
    /// <summary>
    /// Interface for Currency Service
    /// </summary>
    public interface ICurrencyService
    {
        Task<CurrencyConversion> GetCurrency(string sourceCurrency);
    }
}
