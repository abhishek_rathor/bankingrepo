﻿using BankingApp.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace BankingApp.Services
{
    /// <summary>
    /// Currency Convertor Service
    /// </summary>
    public class CurrencyService : ICurrencyService
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Constructor to initialize httpclient and configuration
        /// </summary>
        /// <param name="httpClient"></param>
        /// <param name="configuration"></param>
        public CurrencyService(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            _configuration = configuration;
        }

        /// <summary>
        /// Method to Convert Source currency to AUD
        /// </summary>
        /// <param name="sourceCurrency">Currently only USD supported (free license issue)</param>
        /// <returns></returns>
        public async Task<CurrencyConversion> GetCurrency(string sourceCurrency)
        {
            var response = await _httpClient.GetAsync($"?access_key={_configuration["CurrentConverter:ApiKey"]}&currencies=AUD&source={sourceCurrency}&format=1");
            if (response.IsSuccessStatusCode)
            {
                var content = JsonSerializer.Deserialize<CurrencyConversion>(response.Content.ReadAsStringAsync().Result);
                if(content.error != null)
                    throw new HttpRequestException($"Error: {content.error.info}");
                return content;
            }
            throw new HttpRequestException("Error: Api did not return success");
        }
    }
}
