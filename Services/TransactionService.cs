﻿using BankingApp.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using BankingApp.Dto;

namespace BankingApp.Services
{
    /// <summary>
    /// Transaction Service
    /// </summary>
    public class TransactionService : ITransactionService
    {
        private readonly HttpClient _httpClient;

        /// <summary>
        /// Constructor to initialize httpclient
        /// </summary>
        /// <param name="httpClient"></param>
        public TransactionService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        /// <summary>
        /// Method to get transaction summary withing a date range
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public async Task<IEnumerable<BaseAccount>> GetTransactions(DateTime fromDate, DateTime toDate)
        {
            var response = await _httpClient.GetAsync("/myprosperity.com.au/Test/Transactions.json");
            if (response.IsSuccessStatusCode)
            {
                var content = JsonSerializer.Deserialize<IEnumerable<TransactionDetail>>(response.Content.ReadAsStringAsync().Result);
                var query = from x in content
                            where x.TransactionDate >= fromDate.Date && x.TransactionDate <= toDate.Date
                            group x by x.AccountId into g
                            
                            select new Account
                            {
                                AccountId = g.First().AccountId,
                                AccountName = g.First().AccountName,
                                Balance = g.Sum(amt => amt.Amount)
                            };

                return query;
                    
            }
            throw new HttpRequestException("Error", new Exception("Api did not return success"), response.StatusCode);
        }
    }
}
