﻿using BankingApp.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BankingApp.Services
{
    public interface ITransactionService
    {
        Task<IEnumerable<BaseAccount>> GetTransactions(DateTime fromDate, DateTime toDate);
    }
}